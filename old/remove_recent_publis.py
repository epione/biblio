#!/usr/bin/env python
"""
Remove entries in bibtex file that are HAL-generated and post 2013 (included).
"""

import re

ORIGINAL_FILE="biblio-asclepios.bib"

original_str = ""

with open(ORIGINAL_FILE, 'r') as fh:
    for line in fh:
        if not re.search(r"^\s*\%", line):
            original_str += line

entries = original_str.split("\n@")

new_str_list = []

not_hal = 0
remove = 0

for entry in entries:
    # is_HAL = re.search(r'\{[a-zA-Z]*\:hal\-[0-9]*,', entry)
    is_post2013 = re.search(r'[Yy][Ee][Aa][Rr].*201[3-7]', entry)
    if is_post2013:
        continue
    else:
        print("@" + entry)
