/*************************************************************************
 * custom-common.c - from bibtex2html distribution
 *
 * $Id: custom-common.c,v 2.4 2004/05/06 13:12:26 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


#include <custom-common.h>

int add_tag_element( typeArgDescription *custom,
		     int l,
		     char *key,
		     typeTagElement *tag )
{
  int j = 0;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tag->start);
  }
  j++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tag->end);
  }
  j++;
  
  return( j );
}



void init_tag_element( typeTagElement *t )
{
  t->start = NULL;
  t->end   = NULL;
}



void free_tag_element( typeTagElement *t )
{
  if ( t->start != NULL ) FREE( t->start, "free_tag_element" );
  if ( t->end   != NULL ) FREE( t->end,   "free_tag_element" );

  init_tag_element( t );
}



void print_tag_element( FILE *file, char *key, typeTagElement *tag )
{
  if ( key != NULL ) {
    if ( tag->start != NULL )
      fprintf( file, "%s.start = '%s'\n", key, tag->start );
    else 
      fprintf( file, "%s.start = 'NULL'\n", key );
    if ( tag->end != NULL )
      fprintf( file, "%s.end = '%s'\n", key, tag->end );
    else 
      fprintf( file, "%s.end = 'NULL'\n", key );
  }
  else {
    if ( tag->start != NULL )
      fprintf( file, "tag.start = '%s'\n", tag->start );
    else 
      fprintf( file, "tag.start = 'NULL'\n" );
    if ( tag->end != NULL )
      fprintf( file, "tag.end = '%s'\n", tag->end );
    else 
      fprintf( file, "tag.end = 'NULL'\n" );
  }
    
}



int add_env_element( typeArgDescription *custom,
		     int l,
		     char *key,
		     typeEnvElement *env )
{
  int j = 0;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.use_icon", key );
    custom[ l+j ].type = _INT_;
    custom[ l+j ].arg  = &(env->use_icon);
  }
  j++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.icon", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->icon);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.link_field.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->link_field.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.link_field.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->link_field.end);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.link", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->link);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.content_field.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->content_field.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.content_field.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->content_field.end);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.prefix", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->prefix);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.content.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->content.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.content.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(env->content.end);
  }
  j ++;
  
  return( j );
}



void init_env_element( typeEnvElement *e )
{
  e->use_icon = 0;
  e->icon     = NULL;
  init_tag_element( &(e->link_field) );
  e->link     = NULL;
  init_tag_element( &(e->content_field) );
  e->prefix   = NULL;
  init_tag_element( &(e->content) );
}



void free_env_element( typeEnvElement *e )
{
  if ( e->icon != NULL ) FREE( e->icon, "free_env_element" );
  free_tag_element( &(e->link_field) );
  if ( e->link != NULL ) FREE( e->link, "free_env_element" );
  free_tag_element( &(e->content_field) );
  if( e->prefix != NULL ) FREE( e->prefix, "free_env_element" );
  free_tag_element( &(e->content) );

  init_env_element( e );
}



void print_env_element( FILE *file, char *key, typeEnvElement *env )
{
  if ( key != NULL ) {
    fprintf( file, "%s.use_icon = '%d'\n", key, env->use_icon );
    if ( env->icon != NULL )
      fprintf( file, "%s.icon = '%s'\n", key, env->icon );
  }
  else {
    fprintf( file, "env.use_icon = '%d'\n", env->use_icon );
    if ( env->icon != NULL )
      fprintf( file, "env.icon = '%s'\n", env->icon );
  }    
  print_tag_element( file, key, &(env->link_field) );
  if ( key != NULL ) {
    if ( env->link != NULL )
      fprintf( file, "%s.link = '%s'\n", key, env->link );
  }
  else {
    if ( env->link != NULL )
      fprintf( file, "env.link = '%s'\n", env->link );
    
  }
  print_tag_element( file, key, &(env->content_field) );
  if ( key != NULL ) {
    if ( env->prefix != NULL )
      fprintf( file, "%s.prefix = '%s'\n", key, env->prefix );
  }
  else {
    if ( env->prefix != NULL )
      fprintf( file, "env.prefix = '%s'\n", env->prefix );
  }
  print_tag_element( file, key, &(env->content) );
}



int add_table_element( typeArgDescription *custom,
		       int l,
		       char *key,
		       typeTableElement *tab )
{
  int j = 0;
  
  if ( custom != NULL ) {
    sprintf( custom[ l ].key, "%s.table.start", key );
    custom[ l ].type = _STRING_;
    custom[ l ].arg  = &(tab->table.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.table.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->table.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.row.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->row.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.row.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->row.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->cell.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.empty_cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->empty_cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.empty_cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->empty_cell.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.arg1_in_cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->arg1_in_cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.arg1_in_cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->arg1_in_cell.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.arg2_in_cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->arg2_in_cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.arg2_in_cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->arg2_in_cell.end);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.cells_per_row", key );
    custom[ l+j ].type = _INT_;
    custom[ l+j ].arg  = &(tab->cells_per_row);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.extra_cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->extra_cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.extra_cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->extra_cell.end);
  }
  j ++;

  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.empty_extra_cell.start", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->empty_extra_cell.start);
  }
  j ++;
  
  if ( custom != NULL ) {
    sprintf( custom[ l+j ].key, "%s.empty_extra_cell.end", key );
    custom[ l+j ].type = _STRING_;
    custom[ l+j ].arg  = &(tab->empty_extra_cell.end);
  }
  j ++;

  return( j );
}



void init_table_element( typeTableElement *t )
{
  init_tag_element( &(t->table) );
  init_tag_element( &(t->row) );
  init_tag_element( &(t->cell) );
  init_tag_element( &(t->empty_cell) );
  init_tag_element( &(t->arg1_in_cell) );
  init_tag_element( &(t->arg2_in_cell) );
  t->cells_per_row = -1;
  init_tag_element( &(t->extra_cell) );
  init_tag_element( &(t->empty_extra_cell) );
}



void free_table_element( typeTableElement *t )
{
  free_tag_element( &(t->table) );
  free_tag_element( &(t->row) );
  free_tag_element( &(t->cell) );
  free_tag_element( &(t->empty_cell) );
  free_tag_element( &(t->arg1_in_cell) );
  free_tag_element( &(t->arg2_in_cell) );
  
  free_tag_element( &(t->extra_cell) );
  free_tag_element( &(t->empty_extra_cell) );

  init_table_element( t );
}

void print_table_element( FILE *file, char *key, typeTableElement *t )
{
  print_tag_element( file, key, &(t->table) );
  print_tag_element( file, key, &(t->row) );
  print_tag_element( file, key, &(t->cell) );
  print_tag_element( file, key, &(t->empty_cell) );
  print_tag_element( file, key, &(t->arg1_in_cell) );
  print_tag_element( file, key, &(t->arg2_in_cell) );
  if ( key != NULL ) {
    fprintf( file, "%s.cells_per_row = '%d'\n", key, t->cells_per_row );
  }
  else {
    fprintf( file, "table.cells_per_row = '%d'\n", t->cells_per_row );
  }
  print_tag_element( file, key, &(t->extra_cell) );
  print_tag_element( file, key, &(t->empty_extra_cell) );
}
