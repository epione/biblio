/*************************************************************************
 * write.h - from bibtex2html distribution
 *
 * $Id: write.h,v 2.5 2004/05/19 14:02:25 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include <biblio.h>
#include <sort.h>
#include <custom.h>

#ifndef _write_h_
#define _write_h_

extern int write_string_or_file( FILE *fd, char *s );
extern void write_item_customized_string( FILE *fd, 
				   char *string,
				   typeTagElement *e,
				   char *endstring );

extern int write_all_authors( FILE *index, 
			      typeListOfItems *li, 
			      typeListOfNames *ln, 
			      typeEnvironmentDescription *envdesc,
			      typeGenericFileDescription *gfiledesc,
			      typeSpecificFileDescription *sfiledesc,
			      typeBibtexItemDescription *itemdesc,
			      int write_bibtex_entry );

extern int write_all_keywords( FILE *index, 
			       typeListOfItems *li, 
			       typeListOfNames *ln, 
			       typeEnvironmentDescription *envdesc,
			       typeGenericFileDescription *gfiledesc,
			       typeSpecificFileDescription *sfiledesc,
			       typeBibtexItemDescription *itemdesc,
			       int write_bibtex_entry );

extern int write_all_categories( FILE *index, 
				 typeListOfItems *li, 
				 typeEnvironmentDescription *envdesc,
				 typeGenericFileDescription *gfiledesc,
				 typeSpecificFileDescription *sfiledesc,
				 typeBibtexItemDescription *itemdesc,
				 int write_bibtex_entry );

extern int write_all_years( FILE *index, 
			    typeListOfItems *li, 
			    typeEnvironmentDescription *envdesc,
			    typeGenericFileDescription *gfiledesc,
			    typeSpecificFileDescription *sfiledesc,
			    typeBibtexItemDescription *itemdesc,
			    int write_bibtex_entry );

extern int write_all_items( FILE *fd,  
			    typeListOfItems *list_of_items, 
			    enumSortCriterium c, 
			    typeEnvironmentDescription *envdesc,
			    typeGenericFileDescription *gfiledesc,
			    typeSpecificFileDescription *sfiledesc,
			    typeBibtexItemDescription *itemdesc,
			    int write_bibtex_entry );







extern void write_begin_of_file( FILE *fd, 
				 typeGenericFileDescription *d,
				 typeSpecificFileDescription *s,
				 char *file_title,
				 char *page_title );

extern void write_end_of_file( FILE *fd, typeGenericFileDescription *d, 
			       typeSpecificFileDescription *s );

/*
extern void write_item_url( FILE *fd, typeItem *item );
extern void write_item_postscript( FILE *fd, typeItem *item );
extern void write_item_pdf( FILE *fd, typeItem *item );

extern void write_item_abstract_as_ref( FILE *fd, typeItem *item );
extern void write_item_abstract_in_file( FILE *fd, typeItem *item );
extern void write_item_comments_as_ref( FILE *fd, typeItem *item );
extern void write_item_comments_in_file( FILE *fd, typeItem *item );
extern void write_item_bibtex_entry_as_ref( FILE *fd, typeItem *item );
extern void write_item_bibtex_entry_in_file( FILE *fd, typeItem *item );
*/


extern void write_item( FILE *fd, typeItem *item,
			typeEnvironmentDescription *envdesc,
			typeGenericFileDescription *gfiledesc,
			typeSpecificFileDescription *sfiledesc,
		       	typeBibtexItemDescription *itemdesc,
			int write_bibtex_entry );


extern void print_bibliography( FILE *f, typeListOfItems *li, typeListOfStrings *ls,
				int ac, char *av[] );

extern void set_verbose_on_stderr_in_write();
extern void set_verbose_on_stdout_in_write();
extern void set_verbose_in_write();
extern void unset_verbose_in_write();
extern void set_debug_in_write();
extern void unset_debug_in_write();
#endif
