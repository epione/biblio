/*************************************************************************
 * custom-layout.h - from bibtex2html distribution
 *
 * $Id: custom-layout.h,v 2.6 2004/05/07 12:29:50 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _custom_layout_h_
#define _custom_layout_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <common.h>

#include <custom-common.h>

/******************************
 *   FILE LAYOUT
 ******************************/

/*
  <start_of_file>                           | write_begin_of_file
                                            |
     <start_of_main_title>                  |
        <main_title>                        |
     <end_of_main_title>                    |
                                            |
     <start_of_body>                        |
        <header_of_body>                    |


        <local_menu>

        <start_of_title>
           <title>
        <end_of_title>

        <start_of_subtitle>
           <subtitle>
        <end_of_subtitle>
        <start_of_list>
           <start_of_item>
              <item>
           <end_of_item>
	   ...
        <end_of_list>
        

        <footer_of_body>                    | write_end_of_file
        <separator>                         |
             <disclaimer1>                  |
             <linebreak>                    |
             <disclaimer2>                  |
        <separator>                         |
             <date><author>                 |
        <separator>                         |
             <credits>                      |
     <end_of_body>                          |
                                            |
  <end_of_file>                             |
 */







/* what is needed to describe the common
   layout of all files
 */
typedef struct {
  
  typeTagElement file_tag;
  typeTagElement head_tag;
  typeTagElement file_title_tag;
  typeTagElement body_tag;

  char *header_of_body;
  char *footer_of_body;

  char *header_of_contents;
  char *footer_of_contents;

  typeTagElement page_title_tag;
  typeTagElement page_subtitle_tag;

  typeTagElement list_tag;
  typeTagElement item_tag;

  char *separator;
  char *line_break;
  
  typeEnvElement disclaimer;
  char *disclaimer_1;
  char *disclaimer_2;

  typeTagElement date_tag;

  typeTagElement author_tag;
  char *author_name;

  typeTagElement credits_tag;
  char *credits;

} typeGenericFileDescription;




/* what is needed to describe the specific
   layout of a given file
 */
typedef struct {
  
  int create_pages;
  int write_complete_suffix;

  char *file_title;
  char *default_file_title;

  char *page_title;
  char *default_page_title;

  int  write_local_menu;

  char *header_of_body;
  char *footer_of_body;

  char *header_of_contents;
  char *footer_of_contents;

  int build_initials_index;
  typeTableElement initials;

  int put_initials_in_index;

  int build_index;
  typeTableElement index;

  int write_anchor;
  char *anchor_prefix;

  int  write_disclaimer;
  int  write_date;
  int  write_author;
  int  write_credits;

  int write_bibtex_abstract;
  int write_bibtex_annote;
  int write_bibtex_comments;
  int write_bibtex_key;
  int write_bibtex_file;
  int write_bibtex_entry;

} typeSpecificFileDescription;



/* global constants
 */
extern char *default_specific_file_description[];

extern char *default_generic_file_description_html[];
extern char *default_specific_file_description_html[];
extern char *default_generic_file_description_latex[]; 
extern char *default_specific_file_description_latex[]; 
extern char *default_generic_file_description_txt[]; 
extern char *default_specific_file_description_txt[]; 
extern char *default_generic_file_description_none[]; 
extern char *default_specific_file_description_none[]; 



extern typeArgDescription generic_file_description[];
extern typeArgDescription specific_file_description[];

/* prototypes
 */
extern void init_generic_file_description( typeGenericFileDescription *g );
extern void free_generic_file_description( typeGenericFileDescription *g );

extern void init_specific_file_description( typeSpecificFileDescription *s );
extern void free_specific_file_description( typeSpecificFileDescription *s );


extern int add_generic_file_strings( typeArgDescription *custom,
			      int length,
			      typeGenericFileDescription *f,
			      typeArgDescription *fd );
extern int add_specific_file_strings( typeArgDescription *custom,
			       int length,
			       char *prefix,
			       typeSpecificFileDescription *f,
			       typeArgDescription *fd );


extern int update_file_description_with_default ( typeSpecificFileDescription *f,
						  typeSpecificFileDescription *d );

extern int update_string( char **f, char *d, int force_update );


#ifdef __cplusplus
}
#endif

#endif
