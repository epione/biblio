/*************************************************************************
 * common.h - from bibtex2html distribution
 *
 * $Id: common.h,v 2.1 2004/05/06 12:50:42 greg Exp $
 *
 * Copyright � INRIA 2004, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#ifndef _common_h_
#define _common_h_

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __memory_leaks
#define MALLOC( SIZE, PROC )_malloc( SIZE, PROC )
#define FREE( PTR, PROC ) _free( PTR, PROC )
#else
#define MALLOC( SIZE, PROC ) malloc( SIZE )
#define FREE( PTR, PROC ) free( PTR )
#endif


extern void *_malloc( size_t size,  char *proc );
extern void _free( void *ptr, char *proc );


#ifdef __cplusplus
}
#endif

#endif
